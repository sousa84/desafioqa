Feature:	Pesquisar Medico e Cidade
Scenario:	Preencher pesquisa por medico e validar o resultado

Given um usuario acessar o site da Unimed
When  efetuar uma pesquisa por Medico e Rio de Janeiro
Then  valida se os resultados apresentam Especialidade e Cidade

Scenario:	Preencher pesquisa por medico e validar a cidade exibida

Given um usuario acessar o site da Unimed
When  efetuar uma pesquisa por Medico e Rio de Janeiro
Then  valida se o resultado das 3 primeiras paginas nao exibe Sao Paulo como cidade