$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("caso_teste/funcionalidade.feature");
formatter.feature({
  "name": "Pesquisar Medico e Cidade",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Preencher pesquisa por medico e validar o resultado",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "um usuario acessar o site da Unimed",
  "keyword": "Given "
});
formatter.match({
  "location": "step.um_usuario_acessar_o_site_da_Unimed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "efetuar uma pesquisa por Medico e Rio de Janeiro",
  "keyword": "When "
});
formatter.match({
  "location": "step.efetuar_uma_pesquisa_por_medico_e_rio_de_Janeiro()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida se os resultados apresentam Especialidade e Cidade",
  "keyword": "Then "
});
formatter.match({
  "location": "step.valida_se_os_resultados_apresentam_especialidade_e_cidade()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Preencher pesquisa por medico e validar a cidade exibida",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "um usuario acessar o site da Unimed",
  "keyword": "Given "
});
formatter.match({
  "location": "step.um_usuario_acessar_o_site_da_Unimed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "efetuar uma pesquisa por Medico e Rio de Janeiro",
  "keyword": "When "
});
formatter.match({
  "location": "step.efetuar_uma_pesquisa_por_medico_e_rio_de_Janeiro()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida se o resultado das 3 primeiras paginas nao exibe Sao Paulo como cidade",
  "keyword": "Then "
});
formatter.match({
  "location": "step.valida_se_o_resultado_das_3_primeiras_paginas_nao_exibe_sao_paulo_como_cidade()"
});
formatter.result({
  "status": "passed"
});
});