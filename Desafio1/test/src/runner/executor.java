
	package runner;

	import org.junit.runner.RunWith;

	import cucumber.api.CucumberOptions;

	import cucumber.api.junit.Cucumber;

	
	@RunWith(Cucumber.class)
	@CucumberOptions(features= "caso_teste",
		    glue = {"step"},plugin = { "pretty", "html:target/cucumber-reports" },
		    monochrome = true)
	/**
	 * Executor do Projeto.
	 * 
	 * @author Fabiano de Sousa Mendon�a
	 * @version 0.0.1
	 */
	public class executor {
	
		
	}
	

