package step;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import src.pageObjects;



//import static org.junit.Assert.*;

public class step {	
	
	WebDriver driver;
	
	String url = "https://www.unimed.coop.br/";
	String browser = "Chrome";
	boolean headless = false;
	
	pageObjects paginaPrincipal= new pageObjects(driver);
	
	
	@Given("^um usuario acessar o site da Unimed$")
	public void um_usuario_acessar_o_site_da_Unimed() throws InterruptedException {
		driver = paginaPrincipal.acessar(url,headless);
	}
	
	
	@When("^efetuar uma pesquisa por Medico e Rio de Janeiro$")
	public void efetuar_uma_pesquisa_por_medico_e_rio_de_Janeiro() throws InterruptedException {
		paginaPrincipal.efetuar_pesquisa("M�dico","Rio de Janeiro","Rio de Janeiro");
	}
	
	
	@Then("^valida se os resultados apresentam Especialidade e Cidade$")
	public void valida_se_os_resultados_apresentam_especialidade_e_cidade() {
		paginaPrincipal.valida_especialidade_endereco(paginaPrincipal.retornar_total_registros());
		driver.quit();
	}
	
	@Then("^valida se o resultado das 3 primeiras paginas nao exibe Sao Paulo como cidade$")
	public void valida_se_o_resultado_das_3_primeiras_paginas_nao_exibe_sao_paulo_como_cidade() throws InterruptedException {
		paginaPrincipal.valida_cidade();
		driver.quit();
	}
	
}
