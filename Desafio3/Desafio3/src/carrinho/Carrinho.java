package carrinho;

import java.util.ArrayList;
import java.util.List;

public class Carrinho {
		
	 private List<Produto> produtos;

	    public Carrinho() {
	        produtos = new ArrayList<>();
	    }

	    public void add(Produto produto) {
	        produtos.add(produto);
	    }

	    public Produto menorProduto() throws CarrinhoVazioExpected {
	        if (produtos.isEmpty()) {
	            throw new CarrinhoVazioExpected();
	        }
	        Produto menor = produtos.get(0);
	        for (Produto produto : produtos) {
	            if (produto.getPreco() < menor.getPreco()) {
	                menor = produto;
	            }
	        }
	        return menor;
	    }
	    
	    public double valorCarrinho() throws CarrinhoVazioExpected {
	    	if (produtos.isEmpty()) {
	            throw new CarrinhoVazioExpected();
	        }
			Produto produtosCarrinhos = produtos.get(0);
			double valorTotalCarrinho = 0;
	        for (Produto produto : produtos) {
	        	produtosCarrinhos = produto;
	        	valorTotalCarrinho = valorTotalCarrinho+produtosCarrinhos.getPreco();
	        }
	        //double rvalor = valor.getPreco();
	        return valorTotalCarrinho;
	    }
	    
	    public boolean validaTemAcao() throws CarrinhoVazioExpected {
	    	if (produtos.isEmpty()) {
	            throw new CarrinhoVazioExpected();
	        }
			Produto produtosCarrinho = produtos.get(0);
			boolean temAcao = false;
	        for (Produto produto : produtos) {
	        	produtosCarrinho = produto;
	        	if (produtosCarrinho.getGenero() == "A��o"){
	        		temAcao = true;
	        	}
	        }	    
	        return temAcao;
	    }
	    
	    
	    public int valorDescontoCarrinho() throws CarrinhoVazioExpected {
	    	if (produtos.isEmpty()) {
	            throw new CarrinhoVazioExpected();
	        }
			Produto produtosCarrinho = produtos.get(0);
			int valorTotalCarrinho = 0;
			int valorDesconto = 0;
			boolean temAcao = false;
	        for (Produto produto : produtos) {
	        	produtosCarrinho = produto;
	        	valorTotalCarrinho = valorTotalCarrinho+(int)produtosCarrinho.getPreco();
	        	if (produtosCarrinho.getGenero() == "A��o"){
	        	temAcao = true;
	        	}
	        }	
	        
	       if (temAcao) {
	    	   valorDesconto = 5;
	       }
	       
	       if (valorTotalCarrinho > 100.0 && valorTotalCarrinho < 200.00){
	    	   valorDesconto = valorDesconto + 10;
	       }
	       if (valorTotalCarrinho >= 200.0 && valorTotalCarrinho < 300.00){
	    	   valorDesconto = valorDesconto + 20;
	       }
	       if (valorTotalCarrinho >= 300.0 && valorTotalCarrinho < 400.00){
	    	   valorDesconto = valorDesconto + 25;
	       }
	       if (valorTotalCarrinho >= 400.0){
	    	   valorDesconto = valorDesconto + 30;
	       }
	      
	        //double rvalor = valor.getPreco();
	        return valorDesconto;
	    }
}

