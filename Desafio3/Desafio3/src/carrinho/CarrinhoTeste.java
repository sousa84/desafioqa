package carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import carrinho.Produto;
import carrinho.Carrinho;
import carrinho.CarrinhoVazioExpected;

public class CarrinhoTeste {
    private Carrinho carrinho;

    @Before
    public void criandoCarrinho() {
        carrinho = new Carrinho();
    }

       
 
	@Test
    public void carrinhoTotal() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("Meu malvado Favorito","Anima��o", 200.00);
        carrinho.add(filme1);
        Produto filme2 = new Produto("Velozes e Furiosos 7","A��o", 100.00);
        carrinho.add(filme2);
        Double valorCarrinho,carrinhoAtual;
        carrinhoAtual = filme1.getPreco()+filme2.getPreco();
        valorCarrinho = carrinho.valorCarrinho();
        assertEquals(valorCarrinho, carrinhoAtual);
    //    System.out.println(valorCarrinho);
    }
	
	@Test
	public void validaTemAcao() throws CarrinhoVazioExpected {
        Produto filme = new Produto("Velozes e Furiosos 6","A��o", 55.00);
        carrinho.add(filme);
        boolean validaAcao;
        validaAcao = carrinho.validaTemAcao();
        //valorCarrinho = carrinho.valorCarrinho();
        assertTrue(validaAcao);
        //assertEquals(valorCarrinho, carrinhoAtual);
   //     System.out.println(validaAcao);
    }
	
	@Test
	public void descontoCarrinho05() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("Velozes e Furiosos 6","A��o", 55.00);
        carrinho.add(filme1);
        int valorCarrinho;
        valorCarrinho = carrinho.valorDescontoCarrinho();
        assertEquals(valorCarrinho,5);
   //     System.out.println(valorCarrinho);
    }
	
	@Test
	public void descontoCarrinho10() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("The Scapegoat","Drama", 100.00);
        carrinho.add(filme1);
        Produto filme2 = new Produto("Senhor dos Aneis","Fantasia", 45.00);
        carrinho.add(filme2);
        int valorCarrinho;
        valorCarrinho = carrinho.valorDescontoCarrinho();
        assertEquals(valorCarrinho,10);
   //     System.out.println(valorCarrinho);
    }
	
	@Test
	public void descontoCarrinho20comAcao() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("The Scapegoat","Drama", 100.00);
        carrinho.add(filme1);
        Produto filme2 = new Produto("Velozes e Furiosos 7","A��o", 100.00);
        carrinho.add(filme2);
        int valorCarrinho;
        valorCarrinho = carrinho.valorDescontoCarrinho();
        assertEquals(valorCarrinho,25);
   //     System.out.println(valorCarrinho);
    }
	
	@Test
	public void descontoCarrinho25comAcao() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("Meu malvado Favorito","Anima��o", 200.00);
        carrinho.add(filme1);
        Produto filme2 = new Produto("Velozes e Furiosos 7","A��o", 100.00);
        carrinho.add(filme2);
        int valorCarrinho;
        valorCarrinho = carrinho.valorDescontoCarrinho();
        assertEquals(valorCarrinho,30);
   //     System.out.println(valorCarrinho);
    }
	
	@Test
	public void descontoCarrinho25() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("Meu malvado Favorito","Anima��o", 200.00);
        carrinho.add(filme1);
        Produto filme2 = new Produto("The Scapegoat","Drama", 100.00);
        carrinho.add(filme2);
        int valorCarrinho;
        valorCarrinho = carrinho.valorDescontoCarrinho();
        assertEquals(valorCarrinho,25);
   //     System.out.println(valorCarrinho);
    }
	
	@Test
	public void descontoCarrinho30comAcao() throws CarrinhoVazioExpected {
        Produto filme1 = new Produto("Meu malvado Favorito","Anima��o", 200.00);
        carrinho.add(filme1);
        Produto filme2 = new Produto("Velozes e Furiosos 7","A��o", 100.00);
        carrinho.add(filme2);
        Produto filme3 = new Produto("Velozes e Furiosos 6","A��o", 55.00);
        carrinho.add(filme3);
        Produto filme4 = new Produto("The Scapegoat","Drama", 100.00);
        carrinho.add(filme4);
        int valorCarrinho;
        valorCarrinho = carrinho.valorDescontoCarrinho();
        assertEquals(valorCarrinho,35);
      //  System.out.println(valorCarrinho);
    }

}