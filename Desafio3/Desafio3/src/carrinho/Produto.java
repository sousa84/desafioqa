package carrinho;

public class Produto {
		
		private static int sequence = 0;
	    private String nomefilme;
	    private String genero;
	    private double preco;
		private int idfilme;

	    public Produto(String nomefilme, String genero, double preco) {
	        this.idfilme = sequence++;
	    	this.nomefilme = nomefilme;
	    	this.genero = genero;
	        this.preco = preco;
	    }
	    
	    public int getIdFilmes() {
	    	return idfilme;
	    }
	    
	    public String getNomeFilme() {
	    	return nomefilme;
	    }
	    
	    public double getPreco() {
	        return preco;
	    }
	    
	    public String getGenero() {
	    	return genero;
	    }
	    

	//    @Override
	//    public boolean equals(Object obj) {
	//        return equals((Produto) obj);
	//    }

	//    public boolean equals(Produto obj) {
	 //       return nomefilme.equals(obj.nomefilme);
	 //  }
	
}
